## Quick intro

SNMP examples are not actively maintained and mainly kept for the completeness

Main focus is Nokia SROS based platforms and no plans for Nokia SRLinux examples so far.

### Install SNMP toolset

    apt install snmp

## Minimal SROS configuration

### MD-CLI

    /configure system security snmp community public { access-permissions r }
    /configure system security snmp community public { version both }
    /configure system management-interface snmp admin-state enable

### Classic CLI

    /configure system security snmp community public r version both
    /configure system snmp no shutdown
    /configure system security user "admin" access ftp snmp console netconf grpc

## MIBs installation [SROS example]

    wget <http_path>/Nokia-SR-22.5.R1-MIBs.zip
    unzip Nokia-SR-22.5.R1-MIBs.zip
    mv MIBs/ /usr/share/snmp/mibs/ -R
    echo 'mibdirs /usr/share/snmp/mibs/MIBs:/usr/share/snmp/mibs:/usr/share/snmp/mibs/iana:/usr/share/snmp/mibs/ietf' >> /etc/snmp/snmp.conf

### SNMP examples

- Test that translation works

        snmptranslate -IR -Td -OS -On TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550914

- Get router SW version

        snmpget -v 2c -c public 138.203.18.219 iso.3.6.1.4.1.6527.3.1.2.2.1.8.1.21.1.134217729

- Get a list of interfaces

        snmpwalk -v 2c -c public 138.203.18.219 iso.3.6.1.4.1.6527.3.1.2.2.4.2.1.6.1

## "How to find OID of your interest?"

### Search for a potential name, in this example 'Ports'

    #grep Ports MIBs/numbers.txt
        1.3.6.1.2.1.7.2                             udpNoPorts                                           LEAF  Counter32
        1.3.6.1.4.1.6527.3.1.2.2.1.3.1.8            tmnxChassisNumPorts                                  LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.2.1.3.1.38           tmnxChassisNumPhysicalPorts                          LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.2.3.8.1.7            tmnxMDAMaxPorts                                      LEAF  Integer32
        1.3.6.1.4.1.6527.3.1.2.2.3.8.1.8            tmnxMDAEquippedPorts                                 LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.4.2.3.1.66           svcTlsPriPortsCumulativeFactor                       LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.4.2.3.1.67           svcTlsSecPortsCumulativeFactor                       LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.4.2.95.1.22          svcTmplTlsPriPortsCumFactor                          LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.4.2.95.1.23          svcTmplTlsSecPortsCumFactor                          LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.4.3.47.1.1           msapIgmpSnpgMcacLagPortsDown                         LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.16.37.1.1.10         tQosFpResPlcyPortsHqosMode                           LEAF  INTEGER
        1.3.6.1.4.1.6527.3.1.2.41.5.1.1             tmnxMcacLagPortsDown                                 LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.41.7.1.6             tmnxMcacOperPortsDown                                LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.41.9.1.6             tmnxMcacServOperPortsDown                            LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.59.1.6               tmnxLldpConfigManAddrPortsTable                      NODE
        1.3.6.1.4.1.6527.3.1.2.59.1.6.1             tmnxLldpConfigManAddrPortsEntry                      NODE
        1.3.6.1.4.1.6527.3.1.2.65.1.4.3.1.6         tmnxNatPlL2AwDynResvPorts                            LEAF  Unsigned32
        1.3.6.1.4.1.6527.3.1.2.65.1.16.2.1.15       tmnxNatMapRuleExcludedPorts                          LEAF  Gauge32
        1.3.6.1.4.1.6527.3.1.2.65.1.16.2.1.16       tmnxNatMapRulePortsPerUser                           LEAF  Gauge32

### Looks like we found an object of interest and we need to find corresponding MIB file

    grep tmnxChassisNumPhysicalPorts MIBs/*
        MIBs/TIMETRA-CHASSIS-MIB.mib:        tmnxChassisNumPhysicalPorts

### Get Description for this object

    snmptranslate -IR -Td -OS -On TIMETRA-CHASSIS-MIB::tmnxChassisNumPhysicalPorts
        .1.3.6.1.4.1.6527.3.1.2.2.1.3.1.38
        tmnxChassisNumPhysicalPorts OBJECT-TYPE
          -- FROM       TIMETRA-CHASSIS-MIB
          SYNTAX        Unsigned32
          MAX-ACCESS    read-only
          STATUS        current
          DESCRIPTION   "The value of tmnxChassisNumPhysicalPorts indicates the total number of
                 faceplate ports and connector-ports currently installed for all the
                 pyhsical router chassis in the system. This count does not include the
                 Ethernet ports on the CPM cards that are used for management access."
        ::= { iso(1) org(3) dod(6) internet(1) private(4) enterprises(1) timetra(6527) timetraProducts(3) tmnxSRMIB(1) tmnxSRObjs(2) tmnxHwObjs(2) tmnxChassisObjs(1) tmnxChassisTable(3) tmnxChassisEntry(1) 38 }

### Get data using OID

    snmpwalk -v 2c -c public 138.203.18.219 .1.3.6.1.4.1.6527.3.1.2.2.1.3.1.38
        iso.3.6.1.4.1.6527.3.1.2.2.1.3.1.38.1 = Gauge32: 19

### Get data using Names

    snmpwalk -v 2c -c public 138.203.18.219 TIMETRA-CHASSIS-MIB::tmnxChassisNumPhysicalPorts
