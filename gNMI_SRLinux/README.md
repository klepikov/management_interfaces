## Quick intro

gNMIc tool (<https://gnmic.kmrd.dev/install/>) is used to demonstrate basic gRPC capcabilities on Nokia SRLinux devices

### Install gNMIc

> Check: <https://gnmic.kmrd.dev/install/>

    bash -c "$(curl -sL <https://get-gnmic.kmrd.dev>)"


Section is under development