## Introduction

### Logical diagram

![diagram](images/diagram.png)

## Deployments steps

### Install Docker

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    sudo apt-get update
    sudo apt-get install -y docker-ce

> **_NOTE:_**: always check the latest official installation procedure https://docs.docker.com/engine/install/

### Install docker-compose

    curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose

> **_NOTE:_**: always check the latest official installation procedure https://docs.docker.com/compose/install/

### Add flowcharting plugin to Grafana

    mkdir -p ./telemetry/grafana/plugins/
    cd ./telemetry/grafana/plugins/
    wget https://algenty.github.io/flowcharting-repository/archives/agenty-flowcharting-panel-1.0.0a-SNAPSHOT.zip
    unzip agenty-flowcharting-panel-1.0.0a-SNAPSHOT.zip

> GIT reference: <https://github.com/algenty/grafana-flowcharting>

> Repository: <https://algenty.github.io/flowcharting-repository/ARCHIVES.html>

### Install SNMP toolset (Optional)

    apt install snmp

### Install gNMIc (Optional)

Installation procedure: <https://gnmic.kmrd.dev/install/>

    bash -c "$(curl -sL <https://get-gnmic.kmrd.dev>)"

## Required configuration on every SROS node

### [SROS] Configure NTP [Classic CLI example]

    /configure system time
        ntp
            server <NTP_server_IP> router management prefer
            no shutdown
        exit
        dst-zone CEST
            start last sunday march 02:00
            end last sunday october 03:00
        exit
        zone CET

> **_NOTE:_**: Specify a valid and reachable NTP server

### [SROS] Configure NTP [MD-CLI example]

    /configure system time zone { standard name cet }
    /configure system time dst-zone "CEST" { end week last }
    /configure system time dst-zone "CEST" { end month october }
    /configure system time dst-zone "CEST" { end hours-minutes "03:00" }
    /configure system time dst-zone "CEST" { start week last }
    /configure system time dst-zone "CEST" { start month march }
    /configure system time dst-zone "CEST" { start hours-minutes "02:00" }
    /configure system time ntp { admin-state enable }
    /configure system time ntp { server <NTP_server_IP> router-instance "management" prefer true }

> **_NOTE:_**: Specify a valid and reachable NTP server

### [SROS] Enable gRPC [MD-CLI example]

    /configure system { grpc admin-state enable }
    /configure system { grpc allow-unsecure-connection }

> **_NOTE:_**: Secure connection must be used for production, while unsecure connection is used for demo purposes only

### [SROS] Add user with gRPC access [MD-CLI example]

	/configure global
	/configure system security user-params local-user password { complexity-rules allow-user-name true }
	commit
	/configure system security user-params local-user user "nokia" { password @nokia123 }
	/configure system security user-params local-user user "nokia" { access grpc true }
	/configure system security user-params local-user user "nokia" { console member ["administrative"] }
	commit

## Start/Stop

Execute ./01.start.sh to deploy containers

Execute ./99.stop.sh to destroy containers

> **_NOTE:_**: If you plan to use  'netconf-console', 'gnmic' or 'snmp' only, then deployment of Telegraf/Prometeus/Grafana is not required!

## Grafana access

URL to access Grafana configuration page: http://\<ip_address\>:3000

Administrator login/password: admin/admin

Read only user login/password: show/show

## Telegraf raw data access

For debugging purposes URL access to Telegraf raw data: http://\<ip_address\>:9273/metrics

## Prometheus access

For debugging purposes URL access to Telegraf raw data: http://\<ip_address\>:9090

## SROS verification

    SROS# show system telemetry grpc subscription
    SROS# show system telemetry grpc subscription <id> paths

## SRLinux verification

    SRL# info from state system gnmi-server subscription *

## Telegraf tuning

Telegraf is a powerful tool to subscribe, convert and pre-process gRPC telemetry data. It can also be used to perform SNMP polling.

Main configuration file 'telemetry/telegraf.conf' can be adapted in order to fit your requirements:

- Subscription modes (sample, on-change, once) and XPATH
- Sample intervals, where it's applicable
- SNMP enabled/disabled
- Target IP Addresses and Ports of the nodes

> **_NOTE:_**: Don't forget to define "hostname" in /etc/hosts file

    cat /etc/hosts | grep sros.node.ipd.lab
    192.168.1.100 sros.node.ipd.lab

Each modification of the configuration file requires a container restart.

> **_NOTE:_**: selected by Telegraf timers are applicable for demo purposes and have to be carefully selected for the production!

## gNMIc tuning

gNMIc is an alternative to Telegraf way of gRPC subscription.
Integration is in process and this chapter will be updated soon. 

## Prometheus Alerting system

*The Alertmanager handles alerts sent by client applications such as the Prometheus server*

"Alertmanager" relies on 2 configuration files:

- prometheus/prometheus.yml, which is resoponsible for event triggers (bandwidth utilization, CPU/Memory utilization, etc) 
- alertmanager/config.yaml, which is responsible for delivery channels (Slack, Telegram, Email, etc)

> **_NOTE:_**: alertmanager/config.yam file has to be modified and proper tockens, chat_ids, etc have to be used!
> **_NOTE:_**: alertmanager container is managed by a dedicated telemetry/alertmanager/docker-compose.yaml compose file 

More information about alerting can be found in official documentation using links below

Inspired by:
- https://github.com/prometheus/alertmanager
- https://prometheus.io/docs/alerting/latest/overview/

Alerts examples:
- https://awesome-prometheus-alerts.grep.to/alertmanager

Report to Telegram (Native):
- https://prometheus.io/docs/alerting/latest/configuration/#telegram_config
- https://github.com/prometheus/alertmanager/pull/2827

Create Telegram Bot:
- https://core.telegram.org/bots#creating-a-new-bot

Report to Slack:
- https://grafana.com/blog/2020/02/25/step-by-step-guide-to-setting-up-prometheus-alertmanager-with-slack-pagerduty-and-gmail/

Alerts examples:
- https://github.com/stefanprodan/dockprom
