#!/usr/bin/bash

host="$(grep 'sros.node.ipd.lab' /etc/hosts 2>&1)"
if [ $? -ne 0 ]; then
    echo "[Error] Host 'sros.node.ipd.lab' is not present in /etc/hosts file"
    exit
fi
docker-compose up -d

changeme=$(grep 'changeme' telemetry/alertmanager/config.yml 2>&1)
if [[ $changeme == '' ]]; then
    docker-compose -f telemetry/alertmanager/docker-compose.yaml up -d
    echo "[INFO] Alertmanager is running"
else
    echo "[Warning] Alertmanager is NOT running"
    echo "[Warning] If Alertmanager is required, fix telemetry/alertmanager/config.yml lines:"
    echo "$changeme"
    exit
fi

